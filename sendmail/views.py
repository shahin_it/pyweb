from django.core.mail import send_mail
from django.shortcuts import render

# Create your views here.
from apps import settings
from .forms import SendMailForm


def send(request):
    success = False
    if request.POST:
        form = SendMailForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data.get('subject')
            message = form.cleaned_data.get('message')
            _from = settings.EMAIL_HOST_USER
            recipient = [form.cleaned_data.get('to')]
            send_mail(subject, message, _from, recipient)
            success = True
    email_form = SendMailForm()
    return render(request, "email/email.html", {'email_form': email_form, 'success': success})
