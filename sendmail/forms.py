from django import forms
from django.forms import Form


class SendMailForm(Form):
    to = forms.EmailField(max_length=150, required=True)
    subject = forms.CharField(max_length=150, required=True)
    message = forms.CharField(widget=forms.Textarea(attrs={"rows": 3, "cols": 30}), max_length=1000, required=True)
