from django.urls import path

from . import views
from .views import index

urlpatterns = [
    path('', index),
    path('post-list/', views.post_list, name='post-list'),
    path('single-post/<post_id>/', views.single_post, name='single-post'),
]
