from django.contrib import admin

from .models import Message  # মডেল ইম্পোর্ট করা হল
from .models import Post

# Register your models here.
admin.site.register(Post)
admin.site.register(Message)  # মডেলকে এডমিন সাইটে রেজিস্টার করা হল
