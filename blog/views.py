from django.shortcuts import render

from .models import Message, Post


# Create your views here.

def index(request):
    if Message.objects.count() == 0:
        Message.objects.create(text="Hi there! How are You!")
        Message.objects.create(text="Have a nice day boy!")
        Message.objects.create(text="Are you Crazy !")

    all_messages = Message.objects.all()
    post_list = Post.objects.all()
    return render(request, 'blog/index.html', {'messages': all_messages, 'all_post_list': post_list})


def post_list(request):
    post_list = Post.objects.all()
    return render(request, 'blog/post_list.html', {'post_list': post_list})


def single_post(request, post_id):
    post = Post.objects.get(pk=post_id)
    return render(request, 'blog/single_post.html', {'post': post})
