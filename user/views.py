from django.contrib.auth import authenticate, login, logout
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from user.forms import SignUpForm


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return render(request, 'user/thankyou.html')
        else:
            return HttpResponse("Username or password incorrect")
    elif request.user.is_authenticated:
        return render(request, 'user/thankyou.html')
    return render(request, 'user/login.html')


@transaction.atomic
def user_signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return render(request, 'user/thankyou.html')
    else:
        form = SignUpForm()
    return render(request, 'user/signup.html', {'signup_form': form})


def user_logout(request):
    logout(request)
    return redirect('login')


def home(request):
    return render(request, 'user/home.html')
