from datetime import datetime

from django.utils.deprecation import MiddlewareMixin

from apps import utils


def customContext(request):
    context = {}
    context['appsUrl'] = utils.sleep(lambda: utils.getAppsUrl())
    context['base_url'] = utils.sleep(lambda: utils.baseUrl(request))
    return context


class CustomMiddleWare(MiddlewareMixin):

    # One-time configuration and initialization on start-up
    def __init__(self, get_response=None):
        # One-time configuration and initialization on start-up
        self.get_response = get_response

    def process_request(self, request):
        request._request_time: datetime = datetime.now()

    # def process_view(self, request, view_func, view_args, view_kwargs):
    # Logic executed before a call to view
    # Gives access to the view itself & arguments

    def process_exception(self, request, exception):
        # Logic executed if an exception/error occurs in the view
        print("Exception occurred!")

    def process_template_response(self, request, response):
        return response

    def process_response(self, request, response):
        return response
