# Create your views here.
from django.shortcuts import render

from .models import Districts, Divisions


def district_list(request):
    districts = Districts.objects.all()
    return render(request, 'information/districts.html', {'districts': districts})


def division_list(request):
    divisions = Divisions.objects.all()
    return render(request, 'information/division_list.html', {'divisions': divisions})


def dists_of_division(request, div_id):
    division = Divisions.objects.get(pk=div_id)
    districts = Districts.objects.filter(division=division)
    return render(request, 'information/dist_of_division.html', {'districts': districts, 'division': division, })
