from django.contrib import admin

# Register your models here.
from .models import NidInfo

admin.site.register(NidInfo)
