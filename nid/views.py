from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import NidInfo


def nid_form(request):
    return render(request, "nid/check_nid.html")


@api_view()
def check_nid(request, nid_number):
    is_exist = NidInfo.objects.filter(nid_number=nid_number).exists()
    return Response({'status': is_exist})
