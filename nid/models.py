from django.db import models


# Create your models here.
class NidInfo(models.Model):
    name = models.CharField(max_length=50)
    nid_number = models.CharField(max_length=50)

    def __str__(self):
        return self.name
