from django.urls import path

from . import views

urlpatterns = [
    path('', views.nid_form),
    path('check/<nid_number>', views.check_nid),
]
